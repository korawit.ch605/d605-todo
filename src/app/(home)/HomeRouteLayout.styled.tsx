"use client";

import styled from "styled-components";
import { COLOR } from "~/constants/color";

export const HomePageContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background-color: ${COLOR.WILDSAND_GRAY};
`;
