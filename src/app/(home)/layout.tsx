"use client";

import { useSession } from "next-auth/react";
import { redirect } from "next/navigation";
import { LoadingSpinner } from "~/components";
import * as Styled from "./HomeRouteLayout.styled";

export default function HomeRouteLayout({ children }: { children: React.ReactNode }) {
  const { status } = useSession();

  if (status === "loading") {
    return (
      <Styled.HomePageContainer>
        <LoadingSpinner />
      </Styled.HomePageContainer>
    );
  }

  if (status === "authenticated") {
    redirect("/dashboard");
  }

  return <Styled.HomePageContainer>{children}</Styled.HomePageContainer>;
}
