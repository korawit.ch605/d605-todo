"use client";

import { DashboardPageComponent } from "~/components";

export default function DashboardPage() {
  return <DashboardPageComponent />;
}
