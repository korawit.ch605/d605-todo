"use client";

import { useSession } from "next-auth/react";
import { redirect } from "next/navigation";
import { LoadingSpinner, Navbar } from "~/components";
import * as Styled from "./ProtectedRouteLayout.styled";
import { useGetTask } from "~/hooks";
import { SignOutModal } from "~/components/modals";

export default function ProtectedRouteLayout({ children }: { children: React.ReactNode }) {
  const { status } = useSession({
    required: true,
    onUnauthenticated: () => redirect("/"),
  });

  const { isLoading: isGetTaskInitialLoading } = useGetTask();

  if (status === "loading" || isGetTaskInitialLoading) {
    return (
      <Styled.ProtectedRouteLayout>
        <Styled.ContentSection>
          <LoadingSpinner />
        </Styled.ContentSection>
      </Styled.ProtectedRouteLayout>
    );
  }

  return (
    <Styled.ProtectedRouteLayout>
      <SignOutModal />
      <Navbar />
      <Styled.ContentSection>{children}</Styled.ContentSection>
    </Styled.ProtectedRouteLayout>
  );
}
