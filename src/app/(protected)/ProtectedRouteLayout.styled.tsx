"use client";

import styled from "styled-components";
import { COLOR } from "~/constants/color";

export const ProtectedRouteLayout = styled.div`
  min-height: 100vh;
  max-width: 100vw;
  background-color: ${COLOR.WILDSAND_GRAY};
`;

export const ContentSection = styled.div`
  display: flex;
  min-height: calc(100vh - 60px);
  max-width: 100vw;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  overflow-y: auto;

  @media (max-width: 768px) {
    min-height: 100vh;
  }
`;
