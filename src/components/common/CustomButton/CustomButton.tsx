"use client";

import React, { type HTMLAttributes } from "react";
import { CustomButtonStyled } from "./CustomButton.styled";

export default function CustomButton({
  width,
  height,
  label,
  type,
  onClick,
  ...other
}: {
  width?: string;
  height?: string;
  label: string;
  type: string;
  onClick?: () => void;
} & HTMLAttributes<HTMLButtonElement>) {
  return (
    <CustomButtonStyled
      height={height}
      type={type as "button" | "submit" | "reset" | undefined}
      width={width}
      onClick={onClick}
      {...other}
    >
      {label}
    </CustomButtonStyled>
  );
}
