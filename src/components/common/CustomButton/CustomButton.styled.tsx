import styled from "styled-components";
import { COLOR } from "~/constants/color";

type CustomButtonProps = {
  width?: string;
  height?: string;
};

export const CustomButtonStyled = styled.button<CustomButtonProps>`
  width: ${(props) => props.width ?? "64px"};
  height: ${(props) => props.height ?? "36px"};
  border-radius: 999px;
  border: 0px solid ${COLOR.BUTTERFLY_VIOLET};
  color: ${COLOR.SOLID_WHITE};
  background-color: ${COLOR.BUTTERFLY_VIOLET};

  &:hover {
    background-color: ${COLOR.LAVENDER_VIOLET};
  }
`;
