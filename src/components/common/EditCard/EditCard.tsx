import React, { type HTMLAttributes } from "react";
import * as Styled from "./EditCard.styled";
import BaseCard from "~/components/common/BaseCard/BaseCard";
import CustomButton from "~/components/common/CustomButton/CustomButton";

export default function EditCard({
  label,
  name,
  onClickSave,
  ...other
}: {
  label: string;
  name: string;
  onClickSave: () => void;
} & HTMLAttributes<HTMLInputElement>) {
  return (
    <BaseCard pb="5px" pr="6px" pt="5px">
      <Styled.EditCard name={name} placeholder={label} {...other} />
      <CustomButton label={" Save"} type={"submit"} onClick={onClickSave} />
    </BaseCard>
  );
}
