"use client";

import React, { useEffect, useRef, useState } from "react";
import { type FilterDropdownType, useFilterDropdownContext } from "~/contexts/filterDropdownContext";
import { Box, Text } from "~/styles/globals.styled";
import { DropdownContainer, DropdownButton, DropdownContent, DropdownItem } from "./Dropdown.styled";
import { ArrowDownIcon } from "~/components";

const Dropdown = () => {
  const { setFilter } = useFilterDropdownContext();

  const options = [
    { value: "all", label: "All" },
    { value: "done", label: "Done" },
    { value: "undone", label: "Undone" },
  ];

  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(options[0]);
  const dropdownRef = useRef<HTMLDivElement>(null);

  const handleClickOutside = (event: MouseEvent) => {
    if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
      setIsOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = (option: (typeof options)[0]) => {
    setSelectedOption(option);
    setFilter(option.value as FilterDropdownType);
    setIsOpen(false);
  };

  return (
    <DropdownContainer ref={dropdownRef}>
      <DropdownButton onClick={toggleDropdown}>
        <Box alignItems="center" display="flex" justifyContent="space-between" w="100%">
          <Text fontSize="13px">{selectedOption?.label}</Text>
          <ArrowDownIcon />
        </Box>
      </DropdownButton>
      <DropdownContent isOpen={isOpen}>
        {options.map((option) => (
          <DropdownItem
            key={option.value}
            isSelected={option.label === selectedOption?.label}
            onClick={() => handleOptionClick(option)}
          >
            {option.label}
          </DropdownItem>
        ))}
      </DropdownContent>
    </DropdownContainer>
  );
};

export default Dropdown;
