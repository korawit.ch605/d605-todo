import React from "react";
import * as Styled from "./LoadingSpinner.styled";
import { Text } from "~/styles/globals.styled";

const LoadingSpinner = () => {
  return (
    <React.Fragment>
      <Styled.SpinnerContainer>
        {Array(3)
          .fill(null)
          .map((arr, idx) => (
            <Styled.SpinnerDot key={`spinner-${idx}`} />
          ))}
      </Styled.SpinnerContainer>
      <Text>Loading ...</Text>
    </React.Fragment>
  );
};

export default LoadingSpinner;
