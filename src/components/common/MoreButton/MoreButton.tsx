"use client";

import React, { useEffect, useRef, useState } from "react";
import { COLOR } from "~/constants/color";
import { DropdownContainer, DropdownButton, DropdownContent, DropdownItem } from "./MoreButton.styled";
import { MoreIcon } from "~/components";

const MoreButton = ({ onClickEdit, onClickDelete }: { onClickEdit: () => void; onClickDelete: () => void }) => {
  const options = [
    {
      value: "edit",
      label: "Edit",
      color: COLOR.DARK_GRAY,
      onClick: onClickEdit,
    },
    {
      value: "delete",
      label: "Delete",
      color: COLOR.NEWYORK_PINK,
      onClick: onClickDelete,
    },
  ];

  const [isOpen, setIsOpen] = useState(false);
  const dropdownRef = useRef<HTMLDivElement>(null);

  const handleClickOutside = (event: MouseEvent) => {
    if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
      setIsOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = () => {
    setIsOpen(false);
  };

  return (
    <DropdownContainer ref={dropdownRef}>
      <DropdownButton onClick={toggleDropdown}>
        <MoreIcon />
      </DropdownButton>
      <DropdownContent isOpen={isOpen}>
        {options.map((option) => (
          <DropdownItem
            key={option.value}
            color={option.color}
            onClick={() => {
              handleOptionClick();
              option?.onClick();
            }}
          >
            {option.label}
          </DropdownItem>
        ))}
      </DropdownContent>
    </DropdownContainer>
  );
};

export default MoreButton;
