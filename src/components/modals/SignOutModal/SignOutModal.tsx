import { Box, Text } from "~/styles/globals.styled";
import React from "react";
import { CustomButton } from "~/components";
import CustomModal from "~/components/common/CustomModal/CustomModal";
import { signOut } from "next-auth/react";
import { useSignOutModalContext } from "~/contexts/signOutModalContext";

export default function SignOutModal() {
  const { isSignOutModalOpen, setIsSignOutModalOpen } = useSignOutModalContext();
  const handleSignOut = () => {
    signOut({ callbackUrl: "/" });
  };

  return (
    <CustomModal isOpen={isSignOutModalOpen}>
      <Text fontSize="18px">Are you sure you want to sign out?</Text>
      <Box display="flex" gap="16px" justifyContent="center" mt="30px" w="100%">
        <CustomButton
          height="46px"
          label={"Cancel"}
          type={"button"}
          width="120px"
          onClick={() => setIsSignOutModalOpen(false)}
        />
        <CustomButton height="46px" label={"Sign out"} type={"button"} width="120px" onClick={handleSignOut} />
      </Box>
    </CustomModal>
  );
}
