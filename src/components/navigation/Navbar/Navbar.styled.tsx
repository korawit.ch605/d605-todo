import { motion } from "framer-motion";
import styled from "styled-components";

export const NavbarContainer = styled(motion.div)`
  display: flex;
  justify-content: right;
  align-items: center;
  padding: 10px 20px;
  background-color: transparent;
  height: 60px;

  @media (max-width: 768px) {
    display: none;
  }
`;

export const Avatar = styled.img`
  width: 36px;
  height: 36px;
  border-radius: 50%;
`;
