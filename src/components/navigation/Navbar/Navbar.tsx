import React from "react";
import CustomButton from "~/components/common/CustomButton/CustomButton";
import { useSession } from "next-auth/react";
import { Box, Text } from "~/styles/globals.styled";
import { NavbarContainer, Avatar } from "./Navbar.styled";
import { useSignOutModalContext } from "~/contexts/signOutModalContext";

const Navbar: React.FC = () => {
  const { data: session } = useSession();
  const { setIsSignOutModalOpen } = useSignOutModalContext();

  return (
    <NavbarContainer animate={{ opacity: 1, y: 0 }} exit={{ opacity: 0, y: 20 }} initial={{ opacity: 0, y: 20 }}>
      <Box alignItems="center" display="flex" gap="12px">
        <Text>{session?.user?.name}</Text>
        <Avatar alt="Avatar" src={session?.user?.image ?? ""} />
        <CustomButton
          height="40px"
          label={"Sign out"}
          type={"button"}
          width="96px"
          onClick={() => setIsSignOutModalOpen(true)}
        />
      </Box>
    </NavbarContainer>
  );
};

export default Navbar;
