import styled from "styled-components";
import { COLOR } from "~/constants/color";

type SocialButtonStyledProps = {
  bgColor: string;
};
export const LoginForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${COLOR.SOLID_WHITE};
  padding: 30px;
  border-radius: 20px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
`;

export const Title = styled.h1`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 8px;
  background: linear-gradient(to right, ${COLOR.BUTTERFLY_VIOLET}, ${COLOR.NEWYORK_PINK});
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  margin: 16px;
`;

export const SocialButton = styled.div<SocialButtonStyledProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 280px;
  height: 56px;

  border-radius: 20px;
  background-color: ${(props) => props.bgColor};
  border: none;
  cursor: pointer;

  &:hover {
    background-color: ${COLOR.BUTTERFLY_VIOLET};
  }
`;
