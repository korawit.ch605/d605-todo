"use client";

import { signIn } from "next-auth/react";
import { COLOR } from "~/constants/color";
import { Box, Text } from "~/styles/globals.styled";
import { LoginForm, Title, SocialButton } from "./LoginPageComp.styled";
import { DiscordIcon, GoogleIcon, TodoIcon } from "~/components";
import { useSearchParams } from "next/navigation";

const LoginPageComp: React.FC = () => {
  const error = useSearchParams().get("error");

  const handleGoogleLogin = async () => {
    await signIn("google", { callbackUrl: "/dashboard" });
  };

  const handleDiscordLogin = async () => {
    await signIn("discord", { callbackUrl: "/dashboard" });
  };

  return (
    <LoginForm>
      <Box display="flex" flexDir="column" gap="8px">
        <Title>
          <TodoIcon />
          D²Do App
        </Title>
        <Box alignItems="center" display="flex" justifyContent="center" mb="16px">
          <Text color={COLOR.DARK_GRAY} fontSize="18px">
            Bring your to-do list anywhere
          </Text>
        </Box>
        <Box display="flex" flexDir="column" gap="8px">
          <SocialButton bgColor="#131314" onClick={handleGoogleLogin}>
            <Box mr="8px" w="20px">
              <GoogleIcon />
            </Box>
            <Text color="white">Sign in with Google</Text>
          </SocialButton>
          <SocialButton bgColor="#5865F2" onClick={handleDiscordLogin}>
            <Box mr="8px" w="20px">
              <DiscordIcon />
            </Box>
            <Text color="white">Sign in with Discord</Text>
          </SocialButton>
          {error === "OAuthAccountNotLinked" && (
            <Box alignItems="center" display="flex" flexDir="column" justifyContent="center" mt="16px">
              <Text color={COLOR.NEWYORK_PINK} fontSize="16px">
                *Cannot the same email with
              </Text>
              <Text color={COLOR.NEWYORK_PINK} fontSize="16px">
                different providers*
              </Text>
            </Box>
          )}
          {error && error !== "OAuthAccountNotLinked" && (
            <Box alignItems="center" display="flex" flexDir="column" justifyContent="center" mt="16px">
              <Text color={COLOR.NEWYORK_PINK} fontSize="16px">
                *Something went wrong!
              </Text>
              <Text color={COLOR.NEWYORK_PINK} fontSize="16px">
                Please try again*
              </Text>
            </Box>
          )}
        </Box>
      </Box>
    </LoginForm>
  );
};

export default LoginPageComp;
