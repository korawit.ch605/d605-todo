"use client";

import React, { useEffect, useRef, useState } from "react";
import type { UseFormReturn } from "react-hook-form";
import { ProgressCard, AddCard, TaskCard, Dropdown, HookFormWrapper, LoadingOverlaySpinner } from "~/components";
import { useGetTask, useMutateTask } from "~/hooks";
import { useFilterDropdownContext, useSignOutModalContext } from "~/contexts";
import { COLOR } from "~/constants/color";
import { addTaskSchema, editTaskSchema } from "~/schemas/task";
import type { AddTaskReqBodyType, DeleteTaskReqBodyType, EditTaskReqBodyType } from "~/schemas/task";
import { Box, Text } from "~/styles/globals.styled";
import * as Styled from "./DashboardPageComp.styled";

export default function DashboardPageComponent() {
  const { data: taskData, isFetching: isGetTaskFetching } = useGetTask();
  const { handleAddTask, handleEditTask, handleDeleteTask, isLoading: isMutateTaskLoading } = useMutateTask();
  const { filter } = useFilterDropdownContext();
  const { isSignOutModalOpen, setIsSignOutModalOpen } = useSignOutModalContext();
  const [progressPercent, setProgressPercent] = useState<number | undefined>(0);
  const [completedTask, setCompletedTask] = useState<number | undefined>(0);

  const addFormRef = useRef<UseFormReturn>(null);

  useEffect(() => {
    const newCompletedTask = taskData?.filter((task) => task.completed).length;
    setCompletedTask(newCompletedTask);
  }, [taskData]);

  useEffect(() => {
    const newProgressPercent = ((completedTask ?? 0) / (taskData?.length ?? 1)) * 100;
    setProgressPercent(newProgressPercent);
  }, [completedTask, taskData]);

  const filteredTaskData = () => {
    if (filter === "done") {
      return taskData?.filter((task) => task.completed);
    }

    if (filter === "undone") {
      return taskData?.filter((task) => !task.completed);
    }

    return taskData;
  };

  const addFormDefaultValue = {
    title: null,
    completed: false,
  };

  const onSubmitAddTaskForm = async (_formVal: AddTaskReqBodyType) => {
    await handleAddTask.mutateAsync(_formVal);
    addFormRef?.current?.reset();
  };

  const onSubmitEditTaskForm = (_formVal: EditTaskReqBodyType) => {
    const { id, title } = _formVal;
    handleEditTask.mutate({
      id,
      title: title === null || title === "" ? title ?? undefined : title ?? undefined,
    });
  };

  const onCheckedTask = (_val: EditTaskReqBodyType) => {
    const { id, completed } = _val;
    handleEditTask.mutate({
      id,
      completed: !completed,
    });
  };

  const onDeleteTask = (_val: DeleteTaskReqBodyType) => {
    const { id } = _val;
    handleDeleteTask.mutate({ id });
  };

  return (
    <React.Fragment>
      {(isGetTaskFetching || isMutateTaskLoading) && <LoadingOverlaySpinner />}
      <Box mb="60px" mt="32px">
        <ProgressCard completed={completedTask ?? 0} percent={progressPercent ?? 0} />
        <Box display="flex" flexDir="column" gap="16px" mt="32px">
          <Box
            animate={{ opacity: 1, y: 0 }}
            display="flex"
            exit={{ opacity: 0, y: 20 }}
            h="100%"
            initial={{ opacity: 0, y: 20 }}
            justifyContent="space-between"
            w="100%"
          >
            <Text color={COLOR.SOLID_BLACK} fontSize="24px" fontWeight={500}>
              Task
            </Text>
            <Dropdown />
          </Box>
          {filteredTaskData()?.map((task, idx) => (
            <HookFormWrapper
              key={`task-card-form-${idx}`}
              defaultValues={task as EditTaskReqBodyType}
              schema={editTaskSchema}
              onSubmit={onSubmitEditTaskForm}
            >
              <TaskCard
                isChecked={task.completed}
                label={task.title ?? ""}
                name="title"
                onChecked={() => onCheckedTask({ id: task.id, completed: task.completed })}
                onClickDelete={() => onDeleteTask({ id: task.id })}
              />
            </HookFormWrapper>
          ))}
          <HookFormWrapper
            ref={addFormRef}
            defaultValues={addFormDefaultValue}
            schema={addTaskSchema}
            onSubmit={onSubmitAddTaskForm}
          >
            <AddCard
              name="title"
              placeholder="Add your todo..."
              onClickCard={() => addFormRef.current?.setFocus("title")}
            />
            <Box
              alignItems="center"
              display="flex"
              flexDir="column"
              gap="16px"
              justifyContent="center"
              mt="60px"
              w="100%"
            >
              <Styled.BottomButton height="46px" label={"Submit"} type={"submit"} width="120px" />
              <Styled.BottomButton
                height="46px"
                label={"Sign out"}
                type={"button"}
                width="120px"
                onClick={() => {
                  setIsSignOutModalOpen(!isSignOutModalOpen);
                }}
              />
            </Box>
          </HookFormWrapper>
        </Box>
      </Box>
    </React.Fragment>
  );
}
