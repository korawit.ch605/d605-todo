"use client";

import styled from "styled-components";
import CustomButton from "~/components/common/CustomButton/CustomButton";

export const BottomButton = styled(CustomButton)`
  @media (min-width: 768px) {
    display: none;
  }
`;
