import React from "react";
import { COLOR } from "~/constants/color";

export default function MoreIcon() {
  return (
    <svg fill="none" height="24px" viewBox="0 0 24 24" width="24px" xmlns="http://www.w3.org/2000/svg">
      <circle cx="18" cy="12" fill={COLOR.SANTAS_GRAY} r="1.5" transform="rotate(90 18 12)" />
      <circle cx="12" cy="12" fill={COLOR.SANTAS_GRAY} r="1.5" transform="rotate(90 12 12)" />
      <circle cx="6" cy="12" fill={COLOR.SANTAS_GRAY} r="1.5" transform="rotate(90 6 12)" />
    </svg>
  );
}
