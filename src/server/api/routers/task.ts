import { addTaskSchema as createTaskSchema, deleteTaskSchema, editTaskSchema } from "~/schemas/task";

import { createTRPCRouter, protectedProcedure } from "~/server/api/trpc";

export const taskRouter = createTRPCRouter({
  get: protectedProcedure.query(({ ctx }) => {
    return ctx.db.task.findMany({
      orderBy: { createdAt: "asc" },
      where: { createdBy: { id: ctx.session.user.id } },
    });
  }),

  create: protectedProcedure.input(createTaskSchema).mutation(async ({ ctx, input }) => {
    const { title, completed } = input;

    return ctx.db.task.create({
      data: {
        title,
        completed,
        createdBy: { connect: { id: ctx.session.user.id } },
      },
    });
  }),

  edit: protectedProcedure.input(editTaskSchema).mutation(async ({ ctx, input }) => {
    const { title, completed, id } = input;

    return ctx.db.task.update({
      data: {
        ...(title !== undefined && { title }),
        ...(completed !== undefined && { completed }),
      },
      where: { id },
    });
  }),

  delete: protectedProcedure.input(deleteTaskSchema).mutation(async ({ ctx, input }) => {
    const { id } = input;

    return ctx.db.task.delete({
      where: { id },
    });
  }),
});
