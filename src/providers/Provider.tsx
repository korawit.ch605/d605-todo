"use client";

import React from "react";
import { FilterDropdownProvider } from "~/contexts/filterDropdownContext";
import { SessionProvider } from "next-auth/react";
import { TRPCReactProvider } from "~/trpc/react";
import { SignOutModalProvider } from "~/contexts/signOutModalContext";

export function Providers({ children }: { children: React.ReactNode }) {
  return (
    <SessionProvider>
      <TRPCReactProvider>
        <FilterDropdownProvider>
          <SignOutModalProvider>{children}</SignOutModalProvider>
        </FilterDropdownProvider>
      </TRPCReactProvider>
    </SessionProvider>
  );
}
