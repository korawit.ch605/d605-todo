import { z } from "zod";

export const addTaskSchema = z.object({
  title: z.string().nullable(),
  completed: z.boolean(),
});

export const editTaskSchema = z.object({
  id: z.number(),
  title: z.string().optional().nullable(),
  completed: z.boolean().optional(),
});

export const deleteTaskSchema = z.object({
  id: z.number(),
});

export type AddTaskReqBodyType = z.infer<typeof addTaskSchema>;
export type EditTaskReqBodyType = z.infer<typeof editTaskSchema>;
export type DeleteTaskReqBodyType = z.infer<typeof deleteTaskSchema>;
