"use client";

import React, { createContext, useState, useContext, type ReactNode, type Dispatch, type SetStateAction } from "react";

type SignOutModalContextType = {
  isSignOutModalOpen: boolean;
  setIsSignOutModalOpen: Dispatch<SetStateAction<boolean>>;
};

const SignOutModalContext = createContext<SignOutModalContextType | undefined>(undefined);

export const useSignOutModalContext = (): SignOutModalContextType => {
  const context = useContext(SignOutModalContext);

  if (!context) {
    throw new Error("useSignOutModalContext must be used within a SignOutModalProvider");
  }

  return context;
};

export const SignOutModalProvider = ({ children }: { children: ReactNode }) => {
  const [isSignOutModalOpen, setIsSignOutModalOpen] = useState(false);

  const value = {
    isSignOutModalOpen,
    setIsSignOutModalOpen,
  };

  return <SignOutModalContext.Provider value={value}>{children}</SignOutModalContext.Provider>;
};
