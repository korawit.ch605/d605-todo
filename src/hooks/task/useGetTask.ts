"use client";

import { api } from "~/trpc/react";

export function useGetTask() {
  return api.task.get.useQuery();
}
