import { api } from "~/trpc/react";

export function useMutateTask() {
  const utils = api.useUtils();

  const handleAddTask = api.task.create.useMutation({ onSuccess: async () => await utils.task.invalidate() });

  const handleEditTask = api.task.edit.useMutation({ onSuccess: async () => await utils.task.invalidate() });

  const handleDeleteTask = api.task.delete.useMutation({ onSuccess: async () => await utils.task.invalidate() });

  const isLoading = handleAddTask.isLoading || handleDeleteTask.isLoading || handleEditTask.isLoading;

  return { handleAddTask, handleEditTask, handleDeleteTask, isLoading };
}
